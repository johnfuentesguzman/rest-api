const { validationResult } = require('express-validator/check'); // to resturn errors in case the request parameters are not the expected.. see routes/feed.js

const Post = require('../models/post'); // MODEL/SCHEMA OF DB
const User = require('../models/user'); // MODEL/SCHEMA OF DB

// GET All Posts
exports.getPosts = async (req, res, next) => {
  const currentPage = req.query.page || 1; // pagination parameter as query string
  const perPage = 2;
  let totalItems = 0;
  try {
    const totalItems = await Post.find().countDocuments(); // will back with numbers of collections
    const posts = await Post.find() // this return is to allow access to the promisse "then"
      .skip((currentPage - 1) * perPage) // skyping base on the current page (parameter) == pagination
      .limit(perPage);

    res.status(200).json({
      message: 'Fetched posts successfully.',
      posts: posts,
      totalItems: totalItems
    });
  } catch (error) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};


exports.createPost = (req, res, next) => {
  /** Returning error in case the parameters dont have the value expected. for example the Length */

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed, entered data is incorrect.');
    error.statusCode = 422;
    // this error will be handled by the express middleware errors setup on app.js
    throw error; //
  }
  
  if (!req.file) {
    const error = new Error('No image provided.');
    error.statusCode = 422;
    // this error will be handled by the express middleware errors setup on app.js
    throw error;
  }
  
  const imageUrl = req.file.path;
  const title = req.body.title;
  const content = req.body.content;
  let creator;

  const post = new Post({
    title: title,
    content: content,
    imageUrl: imageUrl,
    creator: req.userId   // userId value set on /middleware/is-auth.js
  });

  // save() is a custom/native hook/function of mongoose
  post.save().then((posted)=> {
    return User.findById(req.userId)
  })
  .then((user) => { // from user.findById()
      creator = user;
      user.posts.push(post); // due to in the user model we have a releation with POST collection.. mongoose will create a array join them
      return user.save();
  })
  .then((result) =>{
    res.status(201).json({
      message: 'Post created successfully!',
      post: post,
      creator: {_id: creator._id, name: creator.name}
  });
  })
  .catch(err => {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    //** using next() due to  previously we are making 'throw error' in this func.
    //... like that is possible to reach and show the  errors using express middleware (see app.js)*/
    next(err);
  });

};

// get just ONE POST
/** Important: i KNOW  that in then and catch statement we must use next instead of throw..
 *  but seeting throw in THEN statements we are leaving the catch statament capture it and send to express middleware (app.js)  */
exports.getPost = (req, res, next) => {
  const postId = req.params.postId;
  Post.findById(postId)
    .then(post => {
      if (!post) {
        const error = new Error('Could not find post.');
        error.statusCode = 404;
        throw error; 
        // despite we are into a then statement, using a trow error, this error will be catched by next() in the catch statement
        // and therefore capture by error express middleware (app.js)
      }
      res.status(200).json({ message: 'Post fetched.', post: post });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};


exports.updatePost = (req, res, next) => {
  const postId = req.params.postId;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed, entered data is incorrect.');
    error.statusCode = 422;
    throw error;
  }
  const title = req.body.title;
  const content = req.body.content;
  let imageUrl = req.body.image;
  if (req.file) {
    imageUrl = req.file.path;
  }
  if (!imageUrl) {
    const error = new Error('No file picked.');
    error.statusCode = 422;
    throw error;
  }
  Post.findById(postId)
    .then(post => {
      if (!post) {
        const error = new Error('Could not find post.');
        error.statusCode = 404;
        throw error;
      }
      // if (imageUrl !== post.imageUrl) {
      //   clearImage(post.imageUrl);
      // }
      post.title = title;
      post.imageUrl = imageUrl;
      post.content = content;
      return post.save();
    })
    .then(result => {
      res.status(200).json({ message: 'Post updated!', post: result });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};


exports.deletePost = (req, res, next) => {
  const postId = req.params.postId;
  Post.findById(postId)
    .then(post => {
      if (!post) {
        const error = new Error('Could not find post.');
        error.statusCode = 404;
        throw error;
      }
      // Check logged in user
      //clearImage(post.imageUrl);
      return Post.findByIdAndRemove(postId);
    })
    .then(result => {
      console.log(result);
      res.status(200).json({ message: 'Deleted post.' });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

const clearImage = filePath => {
  filePath = path.join(__dirname, '..', filePath);
  fs.unlink(filePath, err => console.log(err));
};
