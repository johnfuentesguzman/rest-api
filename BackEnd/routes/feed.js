const express = require('express');
const { body } = require('express-validator/check');
const feedController = require('../controllers/feed');
const isAuth = require('../middleware/is-auth'); // getting auth tokem middleware
const router = express.Router();

// GET /feed/posts -- ALL posts --  just works is isAuth/token (middleware) is present from frontend request parameters
router.get('/posts', isAuth, feedController.getPosts);

// POST /feed/post, with validation for the request parameters using 'express-validator'
router.post(
    '/post',
    isAuth,
    [
      body('title')
        .trim()
        .isLength({ min: 5 }),
      body('content')
        .trim()
        .isLength({ min: 5 })
    ],
    feedController.createPost
  );
  
  // GET /feed/posts/:postId -- just ONE POST
  router.get('/post/:postId', isAuth, feedController.getPost);

  // PUT /feed/posts/:postId -- edit post   
  router.put(
    '/post/:postId',
    isAuth,
    [
      body('title')
        .trim()
        .isLength({ min: 5 }),
      body('content')
        .trim()
        .isLength({ min: 5 })
    ],
    feedController.updatePost
  );
    
  // DELE64 /feed/posts/:postId -- DELETE ONE POST
  router.delete('/post/:postId', isAuth,  feedController.deletePost);
  module.exports = router;
  