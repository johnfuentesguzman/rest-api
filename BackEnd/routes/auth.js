const express = require('express');
const { body } = require('express-validator/check');

const User = require('../models/user');
const authController = require('../controllers/auth');

const router = express.Router();

// array validation using middlaware "express-validator"
router.put(
  '/signup',
  [
    body('email') // request parameter
      .isEmail() // retuns true/false. in case of false we will show the message on withMessage
      .withMessage('Please enter a valid email.')
      .custom((value, { req }) => { // it returns a function.. here using destructuring 
        return User.findOne({ email: value }).then(userDoc => {
          if (userDoc) {
            return Promise.reject('E-Mail address already exists!');
          }
        });
      })
      .normalizeEmail(),
    body('password')  // request parameter
      .trim()
      .isLength({ min: 5 }),
    body('name')  // request parameter
      .trim()
      .not()
      .isEmpty()
  ],
  authController.signup
);

router.post('/login', authController.login);

module.exports = router;
