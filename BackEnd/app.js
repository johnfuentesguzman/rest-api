// const path = require('path');

// const express = require('express');
// const bodyParser = require('body-parser');
// const mongoose = require('mongoose');
// const multer = require('multer'); // to uploap images

// const feedRoutes = require('./routes/feed');

// const app = express();

// // setting uploap images middleware, fileStorage is where (location) where the images will be saved
// const fileStorage = multer.diskStorage({
//   destination: (req, file, cb) => { // location
//     cb(null, 'images');
//   },
//   filename: (req, file, cb) => { // name of file to save in /images folder
//     cb(null, new Date().toISOString() + '-' + file.originalname);
//   }
// });

// const fileFilter = (req, file, cb) => {
//   if (
//     file.mimetype === 'image/png' ||
//     file.mimetype === 'image/jpg' ||
//     file.mimetype === 'image/jpeg'
//   ) {
//     cb(null, true); // no error -  valid file
//   } else {
//     cb(null, false);
//   }
// };

// // registering multer in the node instance
// app.use(
//   multer({ storage: fileStorage, fileFilter: fileFilter }).single('image')
// );

// // app.use(bodyParser.urlencoded()); // x-www-form-urlencoded <form>
// app.use(bodyParser.json()); // application/json

// /** using this to return always for image a reponse with the local images/asset 'duck' */
// app.use('/images', express.static(path.join(__dirname, 'images')));

// app.use((req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', '*');
//   res.setHeader(
//     'Access-Control-Allow-Methods',
//     'OPTIONS, GET, POST, PUT, PATCH, DELETE'
//   );
//   res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
//   next();
// });

// app.use('/feed', feedRoutes);

// // this will be handling and returns to frontend any error send by thow or next();
// app.use((error, req, res, next) => {
//   console.log(error);
//   const status = error.statusCode || 500;
//   const message = error.message;
//   res.status(status).json({ message: message });
// });

// mongoose
//     // 'POST' DB
//   .connect(
//     'mongodb+srv://johnfuentesguzman:kurtco235@cluster0.4dsni.mongodb.net/posts'
//   )
//   .then(result => {
//     app.listen(8080);
//   })
//   .catch(err => console.log(err));


const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const multer = require('multer');

const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');

const app = express();

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString() + '-' + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

// app.use(bodyParser.urlencoded()); // x-www-form-urlencoded <form>
app.use(bodyParser.json()); // application/json
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single('image')
);
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use('/feed', feedRoutes);
app.use('/auth', authRoutes);

// error global function: this will collect all the erros triggered by controllers
app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data
  res.status(status).json({ message: message , data: data});
});

mongoose
  .connect(
    'mongodb+srv://johnfuentesguzman:kurtco235@cluster0.4dsni.mongodb.net/posts'
  )
  .then(result => {
    app.listen(8080);
  })
  .catch(err => console.log(err));
