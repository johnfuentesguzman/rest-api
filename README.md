# README #

MERN (using moongose) project practive how to create REST API (endpoints) and consume them:

We have 2 folders

1. FrontEnd: Contains the Front-End project based on react js. http://localhost:3000/

2. Backend: Contains the endpoints based on node to provide the data to Front-end project. http://localhost:8080/
 
### What will I learn? ###

* Setup the front end using React JS, working on 
* Setup the front end using Node JS
* Using express validator on node project/Back-end:  npm i -save express-validator
* Using mongoose to handle mongo DB : npm i --save mongoose
* backend uploading images using multer: npm i --save multer
* Encrypting/hashing paswords: npm i bcryptjs
* Creating a login using web token / JWT: npm i --save jsonwebtoken
